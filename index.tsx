import { component, React, Style, Children } from 'local/react/component'

export const defaultQuoteStyle: Style = {
  padding: '0.5em 1em',
  borderLeft: '0.25em solid rgba(0, 0, 0, 0.125)'
}

export const defaultQuoteAuthorStyle: Style = {
  padding: '0.5em 1em 0'
}

export const Quote = component.children
  .props<{
    className?: string
    children: Children
    style?: Style
    authorStyle?: Style
    author?: string
  }>({
    style: defaultQuoteStyle,
    authorStyle: defaultQuoteAuthorStyle
  })
  .render(({ className, children, style, author, authorStyle }) => (
    <div>
      <div style={style} className={className}>
        {children}
      </div>
      {author ? <div style={authorStyle}>&mdash; {author}</div> : undefined}
    </div>
  ))
